import requests
from tparser import Parser
from ts_connector import ts_connect
import re
import os
import json


def picarto(video_link):
    """
    Запускаем программу
    """
    result_filename = os.path.basename(video_link) + '.ts'
    html = requests.get(video_link).text
    regex = re.compile('"#vod-player", ({.+})')
    match = re.search(regex, html)
    if not match:
        print('Ссылка на видео не найдена')
        return

    json_string = str(match.group(1))
    parsed_string = json.loads(json_string)
    video_link = parsed_string['vod']
    
    playlist_link = requests.get(video_link).content.decode('utf-8').split('\r\n')
    playlist_link = ''.join([x for x in playlist_link if not x.startswith('#')])

    base_link = video_link.split('index.m3u8')[0] + playlist_link.split('index.m3u8')[0]
    
    playlist = requests.get(base_link + playlist_link).content.decode('utf-8').split('\r\n')
    playlist = list(base_link + x for x in playlist if not x.startswith('#'))

    p = Parser(playlist)
    p.set_threads(10)
    p.run()

    if p.queue.empty():
        print('\r\n')
        ts_connect(result_filename)


if __name__ == '__main__':
    picarto('https://picarto.tv/videopopout/RiceGnat_2018.05.21.00.28.31.flv')

