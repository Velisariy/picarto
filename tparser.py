import requests
import os
import sys
from threading import Thread
from queue import Queue
from urllib.parse import urlparse


class Downloader(Thread):
    """Потоковый загрузчик файлов"""

    def __init__(self, queue):
        """Инициализация потока"""
        Thread.__init__(self)
        self.queue = queue
        self.dir = 'tmp/'

        if not os.path.exists(self.dir):
            os.makedirs(self.dir)

    def run(self):
        """Запуск потока"""
        while True:
            # Получаем url из очереди
            link = self.queue.get()

            # Скачиваем файл
            self.download_file(link)

            # Отправляем сигнал о том, что задача завершена
            self.queue.task_done()

    def log_progress(self):
        size = self.queue.qsize()
        string = 'Файлов в очереди: {}'.format(size)
        sys.stdout.write('\r{}        '.format(string))
        sys.stdout.flush()

    def download_file(self, link):
        filename = os.path.basename(link)
        filename = urlparse(filename).path

        if not filename:
            return

        try:
            req = requests.get(link, stream=True)
        except Exception:
            print('Соединение прервано. Ссылка добавлена обратно в очередь')

            self.queue.put(link)
            return

        self.log_progress()

        with open(self.dir + filename, 'wb') as file:
            for chunk in req.iter_content(chunk_size=1024):
                file.write(chunk)


class Parser:
    def __init__(self, links):
        self.queue = Queue()
        self.links = links

    def run(self):
        for link in self.links:
            self.queue.put(link)
        self.queue.join()

    def set_threads(self, threads=1):
        for i in range(threads):
            t = Downloader(self.queue)
            t.daemon = True
            t.start()
