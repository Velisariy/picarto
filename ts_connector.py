import os
import re
import sys
                  
tmp_directory = 'tmp'
dicrectory = 'content'

def progress(count, total, suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
    sys.stdout.flush()


def tokenize(filename):
    digits = re.compile(r'(\d+)')
    
    return tuple(int(token) if match else token
                 for token, match in
                 ((fragment, digits.search(fragment))
                  for fragment in digits.split(filename)))


def ts_connect(path='result.ts'):
    print('Объединяем файлы')
    
    files = [os.path.join(tmp_directory, x) for x in os.listdir(tmp_directory) if x.endswith('.ts') and x != path]
    files.sort(key=tokenize)
    
    current = 0
    total = len(files)

    if not os.path.exists(dicrectory):
        os.makedirs(dicrectory)
    
    with open(os.path.join(dicrectory, path), 'wb') as outfile:
        for file in files:
            current += 1
            
            with open(file, 'rb') as input_file:
                progress(current, total)

                chunk = input_file.read()
                outfile.write(chunk)
            os.remove(file)

if __name__ == '__main__':
    ts_connect()

